package ir.pchamed.mvp_movie;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
