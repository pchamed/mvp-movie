package ir.pchamed.mvp_movie.showdetailsmovie;

import android.os.Bundle;

import dagger.android.support.DaggerAppCompatActivity;
import ir.pchamed.mvp_movie.R;

public class DetailsMovieActivity extends DaggerAppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_activity);
    }
}
