package ir.pchamed.mvp_movie;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import ir.pchamed.mvp_movie.di.DaggerAppComponent;

public class AppControler extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
