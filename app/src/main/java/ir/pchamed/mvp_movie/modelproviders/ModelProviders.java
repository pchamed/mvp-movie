package ir.pchamed.mvp_movie.modelproviders;

import java.util.List;

import ir.pchamed.mvp_movie.data.api.MainApi;
import ir.pchamed.mvp_movie.model.MainSearchMovie;
import ir.pchamed.mvp_movie.utils.rx.RxSchedulers;
import rx.Observable;

public class ModelProviders {
    MainApi mainApi;
    RxSchedulers rxSchedulers;

    public ModelProviders(MainApi mainApi, RxSchedulers rxSchedulers) {
        this.mainApi = mainApi;
        this.rxSchedulers = rxSchedulers;
    }


    public Observable<List<MainSearchMovie>> provideContentObservable(String json){
        return mainApi.setMovie(json)
                .subscribeOn(rxSchedulers.internet())
                .observeOn(rxSchedulers.androidThread());
    }
}
