package ir.pchamed.mvp_movie.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Nullable;

public final class DetailsMovie {
    @Nullable
    @SerializedName("name")
    private String name;
    
    @Nullable
    @SerializedName("image")
    private MovieImage moveImage;



    public DetailsMovie(String name, MovieImage moveImage) {
        this.name = name;
        this.moveImage = moveImage;
    }
    
    @NonNull
    public MovieImage getMoveImage() {
        return moveImage;
    }
    
    public void setMoveImage(MovieImage moveImage) {
        this.moveImage = moveImage;
    }
    
    @NonNull
    public String getName() {
        return name;
    }
    
    
    public void setName(String name) {
        this.name = name;
    }
}
