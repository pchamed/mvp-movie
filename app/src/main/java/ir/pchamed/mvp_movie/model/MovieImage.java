package ir.pchamed.mvp_movie.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Nullable;

public final class MainSearchMovie {
    @Nullable
    @SerializedName("score")
   private String score;
  
    @Nullable
    @SerializedName("show")
    private DetailsMovie detailsMove;

    public MainSearchMovie(@Nullable String score, @Nullable DetailsMovie detailsMove) {
        this.score = score;
        this.detailsMove = detailsMove;
    }

    @Nullable
    public DetailsMovie getDetailsMove() {
        return detailsMove;
    }
    
    public void setDetailsMove(DetailsMovie detailsMove) {
        this.detailsMove = detailsMove;
    }
    
    @Nullable
    public String getScore() {
        return score;
    }
    
    public void setScore(String score) {
        this.score = score;
    }
}