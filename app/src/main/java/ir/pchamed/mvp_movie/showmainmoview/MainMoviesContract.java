package ir.pchamed.mvp_movie.showmainmoview;

import java.util.List;

import ir.pchamed.mvp_movie.BasePresenter;
import ir.pchamed.mvp_movie.BaseView;
import ir.pchamed.mvp_movie.model.MainSearchMovie;

public interface MainMoviesContract {

    interface view extends BaseView<Presenter>{

        void showContent(List<MainSearchMovie> itemContents);

        void setProgressBarProviderContent(boolean boolProgressbar);

        void setTitleBar(String titleBar);

        boolean isActive();


    }


    interface Presenter extends BasePresenter<view>{

        void provideContent(boolean bProvideContent);

        void dropView();

        void takeView(MainMoviesContract.view view);
    }
}
