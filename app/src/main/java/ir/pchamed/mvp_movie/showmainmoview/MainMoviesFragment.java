package ir.pchamed.mvp_movie.showmainmoview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import ir.pchamed.mvp_movie.R;
import ir.pchamed.mvp_movie.di.ActivityScope;
import ir.pchamed.mvp_movie.model.DetailsMovie;
import ir.pchamed.mvp_movie.model.MainSearchMovie;
import ir.pchamed.mvp_movie.showmainmoview.adapter.ContentAdapter;
import ir.pchamed.mvp_movie.showmainmoview.adapter.ContentItemClick;

@ActivityScope
public class MainMoviesFragment extends DaggerFragment implements MainMoviesContract.view {


    private ContentAdapter contentAdapter;


    @BindView(R.id.recSugar)
    RecyclerView recSugar;

    @Inject
    String urlKind;

    @Inject
    MainMoviesContract.Presenter presenter;



    @Inject
    public MainMoviesFragment() {

    }


    ContentItemClick contentItemClick=new ContentItemClick() {
        @Override
        public void onContentClick(DetailsMovie detailsMovie) {

        }

        @Override
        public void onPhotoClick(DetailsMovie detailsMovie) {

        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentAdapter=new ContentAdapter(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.movies_fragment,container,false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this,root);

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recSugar.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recSugar.setAdapter(contentAdapter);
        presenter.provideContent(true);
        return root;
    }



    @Override
    public void showContent(List<MainSearchMovie> itemContents) {
        contentAdapter.swapAdapter(itemContents);

    }

    @Override
    public void setProgressBarProviderContent(boolean boolProgressbar) {

    }


    @Override
    public void setTitleBar(String titleBar) {

    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setPresenter(MainMoviesContract.Presenter presenter) {

    }
}
