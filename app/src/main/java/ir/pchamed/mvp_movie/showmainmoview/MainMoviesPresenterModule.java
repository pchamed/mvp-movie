package ir.pchamed.mvp_movie.showmainmoview;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import ir.pchamed.mvp_movie.di.ActivityScope;
import ir.pchamed.mvp_movie.di.FragmentScope;

import static ir.pchamed.mvp_movie.showmainmoview.MainMoviesActivity.KIND;

@Module
public abstract class MainMoviesPresenterModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract MainMoviesFragment mainContentFragment();


    @ActivityScope
    @Binds
    abstract MainMoviesContract.Presenter providerPresenter(PresenterMovie presenterContent);




    @Provides
    @ActivityScope
    static String provideUrl(){
        return KIND;
    }


}
