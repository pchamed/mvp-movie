package ir.pchamed.mvp_movie.showmainmoview.adapter;

import ir.pchamed.mvp_movie.model.DetailsMovie;

public interface ContentItemClick {
    void onContentClick(DetailsMovie detailsMovie);
    void onPhotoClick(DetailsMovie detailsMovie);
}
