package ir.pchamed.mvp_movie.showmainmoview;

import android.os.Bundle;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import ir.pchamed.mvp_movie.R;
import ir.pchamed.mvp_movie.utils.ActivityUtils;

public class MainMoviesActivity extends DaggerAppCompatActivity {

    public static final String KIND = "get_category_posts";
    @Inject
    MainMoviesFragment injectMainContentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_activity);


        MainMoviesFragment mainContentFragment=(MainMoviesFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if(mainContentFragment==null){
            mainContentFragment=injectMainContentFragment;
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),mainContentFragment,R.id.contentFrame);
        }
    }
}
