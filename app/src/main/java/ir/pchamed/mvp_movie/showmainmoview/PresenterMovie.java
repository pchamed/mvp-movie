package ir.pchamed.mvp_movie.showmainmoview;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import ir.pchamed.mvp_movie.di.ActivityScope;
import ir.pchamed.mvp_movie.model.MainSearchMovie;
import ir.pchamed.mvp_movie.modelproviders.ModelProviders;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
@ActivityScope
public final class PresenterMovie implements MainMoviesContract.Presenter {

    ModelProviders modelProviders;

    private CompositeSubscription subscription;

    @Nullable
    private MainMoviesContract.view view;




    public static String search ="s";



    @Inject
    public PresenterMovie(@Nullable String urlKind, ModelProviders modelProviders) {
        subscription=new CompositeSubscription();
        this.modelProviders=modelProviders;
    }


    @Override
    public void provideContent(boolean bProvideContent) {
        subscription.add(provideContentList());
    }

    @Override
    public void dropView() {
        subscription.clear();
    }

    @Override
    public void takeView(MainMoviesContract.view view) {
        this.view=view;
    }


    private Subscription provideContentList(){
        return modelProviders.provideContentObservable(search)
                .subscribe(this::outputCategoryList);
    }

    private void outputCategoryList(List<MainSearchMovie> categories) {

        if(view !=null){
            view.showContent(categories);
        }
    }

}
