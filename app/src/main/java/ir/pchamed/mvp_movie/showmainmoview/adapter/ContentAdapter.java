package ir.pchamed.mvp_movie.showmainmoview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.pchamed.mvp_movie.R;
import ir.pchamed.mvp_movie.model.MainSearchMovie;
import ir.pchamed.mvp_movie.model.DetailsMovie;
import ir.pchamed.mvp_movie.model.MovieImage;

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MainSearchMovie> itemContentList=new ArrayList<>();

    public ContentAdapter(Context context) {
        this.context = context;
    }

    public void swapAdapter(List<MainSearchMovie> itemContentList){
        this.itemContentList.clear();
        this.itemContentList.addAll(itemContentList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movies,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MainSearchMovie itemContent=itemContentList.get(position);
        holder.bind(itemContent);
    }

    @Override
    public int getItemCount() {
        if(itemContentList != null&&itemContentList.size()>0){
            return itemContentList.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.thumbnail)
        ImageView thunbnail;
        @BindView(R.id.title)
        TextView title;

        View view;


        public ViewHolder(View itemView) {
            super(itemView);
            this.view=itemView;
            ButterKnife.bind(this,view);

        }

        void bind(MainSearchMovie itemContent){
            DetailsMovie detailsMove=itemContent.getDetailsMove();
            MovieImage moveImage=detailsMove.getMoveImage();

            title.setText(detailsMove.getName());
            Picasso.with(context)
                    .load(moveImage.getImageMedium())
                    .resize(150, 150)
                    .centerCrop()
                    .into(thunbnail);


        }

        @Override
        public void onClick(View view) {

        }
    }
}
