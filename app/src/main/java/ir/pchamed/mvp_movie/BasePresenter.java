package ir.pchamed.mvp_movie;

public interface BasePresenter<T> {


    void takeView(T view);


    void dropView();

}
