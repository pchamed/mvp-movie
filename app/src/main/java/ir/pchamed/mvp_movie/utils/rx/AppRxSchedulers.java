package ir.pchamed.mvp_movie.utils.rx;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.Nullable;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AppRxSchedulers implements RxSchedulers {

    public static Executor backgroundExecutor= Executors.newCachedThreadPool();
    public static Scheduler BACKGROUND_SCHEDULER= Schedulers.from(backgroundExecutor);
    public static Executor internetExecutor=Executors.newCachedThreadPool();
    private static Scheduler INTERNET_SCHEDULERS=Schedulers.from(internetExecutor);

    @Nullable
    private static AppRxSchedulers INSTANCE;


    public AppRxSchedulers() {

    }

    public static synchronized AppRxSchedulers getInstance(){
        if(INSTANCE==null){
            INSTANCE=new AppRxSchedulers();
        }
        return INSTANCE;
    }

    public static Scheduler getInternetSchedulers() {
        return INTERNET_SCHEDULERS;
    }


    @NonNull
    @Override
    public Scheduler runOnBackground() {
        return BACKGROUND_SCHEDULER;
    }

    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @NonNull
    @Override
    public Scheduler compute() {
        return Schedulers.computation();
    }

    @NonNull
    @Override
    public Scheduler androidThread() {
        return AndroidSchedulers.mainThread();
    }

    @NonNull
    @Override
    public Scheduler internet() {
        return getInternetSchedulers();
    }
}
