package ir.pchamed.mvp_movie.utils.rx;





import android.support.annotation.NonNull;

import rx.Scheduler;

public interface RxSchedulers {
    @NonNull
    Scheduler runOnBackground();
   @NonNull
   Scheduler io();
   @NonNull
   Scheduler compute();
   @NonNull
   Scheduler androidThread();
   @NonNull
   Scheduler internet();

}
