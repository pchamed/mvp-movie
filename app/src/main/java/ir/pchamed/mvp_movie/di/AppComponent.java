package ir.pchamed.mvp_movie.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import ir.pchamed.mvp_movie.AppControler;
import ir.pchamed.mvp_movie.data.modules.ApiServiceModules;
import ir.pchamed.mvp_movie.data.modules.NetworkModule;
import ir.pchamed.mvp_movie.data.modules.RxModule;

@Singleton
@Component(modules = {ActivityBindingModule.class
        ,ApplicationModule.class
        , AndroidSupportInjectionModule.class
        ,ApiServiceModules.class
        , NetworkModule.class
        , RxModule.class})
public interface AppComponent extends AndroidInjector<AppControler> {

    @Component.Builder
    interface builder{
        @BindsInstance
        AppComponent.builder application(Application application);
        AppComponent build();
    }
}
