package ir.pchamed.mvp_movie.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ir.pchamed.mvp_movie.showmainmoview.MainMoviesActivity;
import ir.pchamed.mvp_movie.showmainmoview.MainMoviesPresenterModule;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = MainMoviesPresenterModule.class)
    abstract MainMoviesActivity mainContentActivity();


}
