package ir.pchamed.mvp_movie.di;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module

public abstract class ApplicationModule {
    @Binds
    abstract Context context(Application application);


}
