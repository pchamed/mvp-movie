package ir.pchamed.mvp_movie.data.api;

import java.util.List;

import ir.pchamed.mvp_movie.model.MainSearchMovie;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface MainApi {

    @GET("/search/shows?")
    Observable<List<MainSearchMovie>> setMovie(
            @Query("q") String q);

}
