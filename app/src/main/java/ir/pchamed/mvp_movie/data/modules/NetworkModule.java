package ir.pchamed.mvp_movie.data.modules;

import android.content.Context;

import java.io.File;
import java.net.Proxy;

import dagger.Module;
import dagger.Provides;
import ir.pchamed.mvp_movie.utils.rx.AppRxSchedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public  class NetworkModule {
    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache){
        OkHttpClient.Builder builder=new OkHttpClient.Builder();
        builder.readTimeout(60, java.util.concurrent.TimeUnit.SECONDS);
        builder.connectTimeout(60, java.util.concurrent.TimeUnit.SECONDS);
        builder.addInterceptor(httpLoggingInterceptor);
        builder.cache(cache);
        builder.proxy(Proxy.NO_PROXY);
        return builder.build();
    }


    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor(){
        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }


    @Provides
    Cache provideCache(File file){
        return new Cache(file,10*10*1000);
    }

    @Provides
    File provideFile(Context context){
        return context.getFilesDir();
    }


    @Provides
    RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory(){
        return RxJavaCallAdapterFactory.createWithScheduler(AppRxSchedulers.getInternetSchedulers());
    }

    @Provides
    GsonConverterFactory provideGsonConverterFactory(){
        return GsonConverterFactory.create();
    }

}
