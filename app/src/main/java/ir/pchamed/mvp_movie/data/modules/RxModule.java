package ir.pchamed.mvp_movie.data.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ir.pchamed.mvp_movie.utils.rx.AppRxSchedulers;
import ir.pchamed.mvp_movie.utils.rx.RxSchedulers;

@Module
public class RxModule {
    @Provides
    @Singleton
    RxSchedulers provideRxSchedulers(){
        return new AppRxSchedulers();
    }
}
