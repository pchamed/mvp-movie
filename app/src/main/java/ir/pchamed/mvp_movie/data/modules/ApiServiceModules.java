package ir.pchamed.mvp_movie.data.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ir.pchamed.mvp_movie.data.api.MainApi;
import ir.pchamed.mvp_movie.modelproviders.ModelProviders;
import ir.pchamed.mvp_movie.utils.rx.RxSchedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public  class ApiServiceModules {
    public static final String BASE_URL="http://api.tvmaze.com";

    @Singleton
    @Provides
    MainApi provideApiService(OkHttpClient client, GsonConverterFactory factory, RxJavaCallAdapterFactory rxJavaCallAdapterFactory){
        Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(factory)
                .build();

        return retrofit.create(MainApi.class);
    }


    @Provides
    @Singleton
    public ModelProviders providerModelProviders(MainApi api, RxSchedulers rxSchedulers){
        return new ModelProviders(api,rxSchedulers);
    }
}
