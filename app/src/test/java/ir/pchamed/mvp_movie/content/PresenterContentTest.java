package ir.pchamed.mvp_movie.content;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import ir.pchamed.mvp_movie.modelproviders.ModelProviders;
import ir.pchamed.mvp_movie.showmainmoview.MainMoviesContract;
import ir.pchamed.mvp_movie.showmainmoview.PresenterMovie;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PresenterContentTest {



    @Mock
    private ModelProviders modelProviders;

    @Mock
    private MainMoviesContract.view view;

    private PresenterMovie presenterContent;

    @Before
    public void setupPresenterContent(){
        MockitoAnnotations.initMocks(this);
        presenterContent=new PresenterMovie("",modelProviders);
        presenterContent.takeView(view);
        when(view.isActive()).thenReturn(true);
    }


    @Test
    public void loadDataFromRepositoryAndLoad(){
        presenterContent.provideContent(true);
        verify(view,times(2)).setProgressBarProviderContent(true);
    }

}
